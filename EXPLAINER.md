## Example code

HTML:
```
<html>
    <body>
        <textarea id="textarea"></textarea>
        <script src="main.js"></script>
    </body>
</html>
```

Javascript:
```javascript
navigator.representedFiles.openMimeTypes = ["text/plain"];
navigator.representedFiles.saveMimeTypes = ["text/plain"];

var textarea = document.getElementById("textarea");
var documents = {};
var active_id;

navigator.representedFiles.setActionHandler("new", function(id) {
    documents[id] = "";
    textarea.value = "";
});

navigator.representedFiles.setActionHandler("open", function(id, file) {
    var reader = new FileReader();
    reader.onload(function(ev) {
        documents[id] = ev.target.result;
        active_id = id;
        textarea.value = documents[id];
    });
    
    reader.readAsText(file);
    
});

navigator.representedFiles.setActionHandler("write", mimeType, function(id, callback) {
    callback("data": + encodeURI(textarea.value));
});

navigator.representedFiles.setActionHandler("activate", function(id) {
    documents[active_id] = textarea.value;
    active_id = id;
    textarea.value = documents[id];
});

navigator.representedFiles.setActionHandler("close", function(id) {
    delete documents[id];
    if (id === active_id)
    {
        active_id = null;
        textarea.value = "";
    }
});

textarea.addEventListener("change", function() {
    navigator.representedFiles.setEdited();
});
```

## Details
[View mockup](https://app.moqups.com/simonbrown60@gmail.com/6yhlhR6Qez/view)

This proposal would allow webapps to work with local files by providing a tabbed interface above the viewport to keep track of the documents a webapp has open, as well as documents that have not yet been saved. Rather than the webapp modifying files directly, something I believe to be inappropriate to the sandboxed environment of the web, the user agent would trigger events prompting the webapp to open or serialise documents based on user interaction, either clicking on menus in this interface or pressing keyboard shortcuts. Files would only be written to or read from the disk when the user has indicated a clear intention.

The web application would receive events when:

* A file is opened. The webapp would be given a FileReader object so that it can read the file and open it as a document, and an id so that it can keep track of the documents it has open.
* A document needs to be serialised. The webapp would be able to return either a data URI or a blob (though maybe a filewriter should be part of this spec?)
* A document is activated. As this API is designed for multiple documents to be open at once, this would allow the webapp to show the document on screen when the user selects its tab. The webapp would receive its id.
* A document is closed. The webapp can delete it from memory and hide it if it is active.
* The user requests a new document be created (but not saved). The webapp is given an id and must show a blank document on the screen.

When the document is edited (made dirty), the webapp can call `navigator.representedFiles.setEdited()`. However, it would never know whether or not the document is dirty because the `write` event is intended to be free of side-effects, and able to be used by the user agent for things other than saving to the represented file, such as auto-save.

## Differences from writable files
This is a similar problem to that solved by the [writable files proposal](https://github.com/WICG/writable-files). However, I believe it is a better solution because:

* Web applications having writable access to files for an extended period of time presents a security problem. This proposal avoids this by giving the user agent control over the process based on user interaction.
* This would tackle the issue of sites billing "save as" like "download," as it would be clear to the user that the user agent is saving the app's represented file rather than downloading something.
* In native applications, a lot of boilerplate code has to be written for applications to be able to open, save, etc. This proposal would make it a much simpler process for web applications.
* It would save webapps from writing a lot of boilerplate code for opening and saving files and keeping track of open files.
* Features such as a recently opened list could also be implemented at the user agent level, without giving a webapp access to them for extended periods of time. Features such as autosave could also be implemented, and integration with other APIs would be simpler.

## Security and Privacy

As an API based on reading and writing files it may increase the attack surface, however I believe the potential for design holes is limited by the fact that the user agent is in control of the process.

To ensure that the user's files aren't transmitted over an insecure connection, the feature would only be available in secure contexts.