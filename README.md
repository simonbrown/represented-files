# Represented files
View proposals in the [EXPLAINER](EXPLAINER.md).

## Problem
Native applications such as image editors and word processors are able to provide streamlined saving functionality. Once they take ownership of a file, either by opening an existing one or saving a new (or existing) one, the user can save their changes to it without selecting a file again. Web applications are unable to do this. Saving changes to a file requires either opening the save dialog and the user re-selecting the file, or worse, the file being saved to downloads under a new name with the user left to move it back after they are done with the application.